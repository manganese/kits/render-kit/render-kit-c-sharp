using UnityEngine;


namespace Manganese.RenderKit.Interfaces {
  public class Render {
    // Final rendered output
    public Texture2D rendered;
  }
}