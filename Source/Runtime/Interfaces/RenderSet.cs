using UnityEngine;


namespace Manganese.RenderKit.Interfaces {
  public class RenderSet {
    // Final rendered output
    public Texture2D rendered;

    // Diffuse color without lighting
    public Texture2D diffuse;

    // Surface normal direction
    public Texture2D normal;

    // Depth from camera
    public Texture2D depth;
  }
}