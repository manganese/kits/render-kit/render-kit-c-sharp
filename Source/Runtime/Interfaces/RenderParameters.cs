using System;
using System.Collections.Generic;

using UnityEngine;

using Newtonsoft.Json;

using Manganese.SerializationKit.Converters;


namespace Manganese.RenderKit.Interfaces {
  public class RenderParameters {
    // Camera transform
    [JsonConverter(typeof(UnityVector3Converter))]
    public Vector3 relativeCameraPosition;

    [JsonConverter(typeof(UnityQuaternionConverter))]
    public Quaternion relativeCameraRotation;

    // Ambient lighting
    [JsonConverter(typeof(UnityColorConverter))]
    public Color ambientSkyColor;

    [JsonConverter(typeof(UnityColorConverter))]
    public Color ambientEquatorColor;

    [JsonConverter(typeof(UnityColorConverter))]
    public Color ambientGroundColor;

    // Directional lights
    public class DirectionalLightParameters {
      [JsonConverter(typeof(UnityColorConverter))]
      public Color color;

      public float intensity;

      [JsonConverter(typeof(UnityQuaternionConverter))]
      public Quaternion rotation;
    }

    public HashSet<DirectionalLightParameters> directionalLights;

    // Point lights
    public class PointLightParameters {
      [JsonConverter(typeof(UnityColorConverter))]
      public Color color;

      public float intensity;

      [JsonConverter(typeof(UnityVector3Converter))]
      public Vector3 relativePosition;
    }

    public HashSet<PointLightParameters> pointLights;
  }
}