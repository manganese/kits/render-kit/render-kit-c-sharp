using UnityEngine;


namespace Manganese.RenderKit.Interfaces {
  public class OrthographicProjectionSet {
    // Size
    public float size;

    // Directional renders
    public Texture2D north;
    public Texture2D east;
    public Texture2D south;
    public Texture2D west;
  }
}