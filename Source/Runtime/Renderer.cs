using UnityEngine;

using Manganese.RenderKit.Interfaces;


namespace Manganese.RenderKit {
  public class Renderer : MonoBehaviour {
    // References
    public new Camera camera;
    public RenderTexture renderTexture;
    public Transform container;

    // Single texture rendering
    public Texture2D Render(AssetBundle assetBundle, string mainAssetPath, RenderParameters renderParameters) {
      var gameObject = UnityEngine.GameObject.Instantiate(assetBundle.LoadAsset<GameObject>(mainAssetPath));
      gameObject.transform.parent = this.container;
      gameObject.transform.localPosition = new Vector3(0, 0, 0);
      gameObject.transform.localRotation = Quaternion.identity;

      return Render(gameObject, renderParameters);
    }

    public Texture2D Render(GameObject gameObject, RenderParameters renderParameters) {
      return null;
    }

    // Multiple texture rendering
    public RenderSet RenderSet(GameObject gameObject, RenderParameters renderParameters) {
      return null;
    }

    // Orthographic rendering
    public OrthographicProjectionSet RenderOrthographicProjectionSet(GameObject gameObject, RenderParameters renderParameters) {
      return null;
    }
  }
}